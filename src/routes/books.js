'use strict';

// dependencies for the book routes
const express = require('express');
const router = express.Router();
const db = require('../models/db');

// get all of the books in the books table of the database
router.get('/', (req, res) => {
  db.query('SELECT * FROM books ORDER BY book_id ASC', (err, results, fields) => {
    if (!err) {
      console.log('[+] Obtaining Books...');
      res.json(results);
    } else {
      console.error(`[!] Error: ${err}`);
    }
  });
});

// handles post requests to the api
// this should insert a new book into the books table
router.post('/', (req, res) => {
  db.query('INSERT INTO books SET ?', req.body, (err, results) => {
    if (!err) {
      console.log('[+] Book has been added successfully...');
      res.json({ message: 'Book added successfuly!' });
    } else {
      console.error(`[!] Error when adding book: ${err}`);
    }
  });
});

// handles get requests for one of the books
// obtains the data for the book entry based on its id
router.get('/:book_id', (req, res) => {
  let book_id = req.params.book_id;

  db.query('SELECT * FROM books WHERE book_id = ?', book_id, (err, results, fields) => {
    if (!err) {
      console.log('[+] Obtaining book...');
      res.json(results);
    } else {
      console.error(`[!] Error: ${err}`);
    }
   });
});

// handles record updates for the specified id
router.put('/:book_id', (req, res) => {
  let book_id = req.params.book_id;

  db.query('UPDATE books SET ? WHERE book_id = ?', [req.body, book_id], (err, results) => {
    if (!err) {
      console.log('[+] Book updated successfully!');
      res.json({ message: 'Book updated sucessfully!' });
    } else {
      console.error(`[!] Error: ${err}`);
    }
  });
});

// handles deletion of a book record
router.delete('/:book_id', (req, res) => {
  let book_id = req.params.book_id;

  db.query('DELETE FROM books WHERE book_id = ?', book_id, (err, results) => {
    if (!err) {
      console.warn('[-] Book has been deleted!')
      res.json({ message: 'Book has been deleted!' });
    } else {
      console.error(`[!] Error: ${err}`);
    }
  });
});

// module exports for the router
module.exports = router;
