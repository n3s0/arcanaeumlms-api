'use strict';

// dependencies for the genres routes
const express = require('express');
const router = express.Router();
const db = require('../models/db');

// get all of the genres in the genres table of the database
router.get('/', (req, res) => {
  db.query('SELECT * FROM genres ORDER BY genre_id ASC', (err, results, fields) => {
    if (!err) {
      console.log('[+] Obtaining Genres...');
      res.json(results);
    } else {
      console.error(`[!] Error: ${err}`);
    }
  });
});

// handles post requests to the api
// this should insert a new genre into the genres table
router.post('/', (req, res) => {
  db.query('INSERT INTO genres SET ?', req.body, (err, results) => {
    if (!err) {
      console.log('[+] Genre has been added successfully...');
      res.json({ message: 'Genre added successfuly!' });
    } else {
      console.error(`[!] Error when adding genre: ${err}`);
    }
  });
});

// handles get requests for one of the genres
// obtains the data for the genre entry based on its id
router.get('/:genre_id', (req, res) => {
  let genre_id = req.params.genre_id;

  db.query('SELECT * FROM genres WHERE genre_id = ?', genre_id, (err, results, fields) => {
    if (!err) {
      console.log('[+] Obtaining genre...');
      res.json(results);
    } else {
      console.error(`[!] Error: ${err}`);
    }
   });
});

// handles record updates for the specified id
router.put('/:genre_id', (req, res) => {
  let genre_id = req.params.genre_id;

  db.query('UPDATE genres SET ? WHERE genre_id = ?', [req.body, genre_id], (err, results) => {
    if (!err) {
      console.log('[+] Genre updated successfully!');
      res.json({ message: 'Genre updated sucessfully!' });
    } else {
      console.error(`[!] Error: ${err}`);
    }
  });
});

// handles deletion of a genre record
router.delete('/:genre_id', (req, res) => {
  let genre_id = req.params.genre_id;

  db.query('DELETE FROM genres WHERE genre_id = ?', genre_id, (err, results) => {
    if (!err) {
      console.warn('[-] Genre has been deleted!')
      res.json({ message: 'Genre has been deleted!' });
    } else {
      console.error(`[!] Error: ${err}`);
    }
  });
});

// module exports for the router
module.exports = router;
