'use strict';

// dependencies for publisher routes
const express = require('express');
const router = express.Router();
const db = require('../models/db');

// get all of the publishers in the publishers table of the database
router.get('/', (req, res) => {
  db.query('SELECT * FROM publishers ORDER BY publisher_id ASC', (err, results, fields) => {
    if (!err) {
      console.log('[+] Obtaining publishers...');
      res.json(results);
    } else {
      console.error(`[!] Error: ${err}`);
    }
  });
});

// handles post requests to the api
// this should insert a new publisher into the publishers table
router.post('/', (req, res) => {
  db.query('INSERT INTO publishers SET ?', req.body, (err, results) => {
    if (!err) {
      console.log('[+] Publisher has been added successfully!');
      res.json({ message: 'Publisher added successfully!' });
    } else {
      console.error(`[!] Error when adding book: ${err}`);
    }
  });
});

// handles get requests for one of the publishers
// obtains data for the publisher entry based on their id
router.get('/:publisher_id', (req, res) => {
  let publisher_id = req.params.publisher_id;

  db.query('SELECT * FROM publishers WHERE publisher_id = ?' , publisher_id, (err, results) => {
    if(!err) {
      console.log('[+] Obtaining book...');
      res.json(results);
    } else {
      console.error(`[!] Error: ${err}`);
    }
  });
});

// handles record updates to the specified id
router.put('/:publisher_id', (req, res) => {
  let publisher_id = req.params.publisher_id;

  db.query('UPDATE publishers SET ? WHERE publisher_id = ?', [req.body, publisher_id], (err, results) => {
    if (!err) {
      console.log('[+] Publisher updated successfully!');
      res.json({ message: 'Publisher updated successfully!' });
    } else {
      console.error(`[!] Error: ${err}`);
    }
  });
});

// handles deletion of a book record
router.delete('/:publisher_id', (req, res) => {
  let publisher_id = req.params.publisher_id;

  db.query('DELETE FROM publishers WHERE publisher_id = ?', publisher_id, (err, results) => {
    if (!err) {
      console.warn('[+} Publisher deleted!');
      res.json({ message: 'Publisher deleted!'})
    } else {
      console.error(`[!] Error: ${err}`);
    }
  });
});

// module exports for the router
module.exports = router;
