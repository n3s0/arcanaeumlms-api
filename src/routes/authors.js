'use strict';

// dependencies for the authors routes
const express = require('express');
const router = express.Router();
const db = require('../models/db');

// get all authors in the authors table of the database
router.get('/', (req, res) => {
  db.query('SELECT * FROM authors ORDER BY author_id ASC', (err, results, fields) => {
    if (!err) {
      console.log('[+] Obtaining Authors...');
      res.json(results);
    } else {
      console.error(`[!] Error: ${err}`);
    }
  });
});

// add an author to the authors table of the database
// this should insert a new author into the authors table
router.post('/', (req, res) => {
  db.query('INSERT INTO authors SET ?', req.body, (err, results) => {
    if (!err) {
      console.log('[+] Author added successfully');
      res.json({ message: 'Author added successfully!' });
    } else {
      console.error(`[!] Error when adding author: ${err}`);
    }
  });
});

// handles get requests for one of the authors in the authors table
// will obtain the selected author by its id.
router.get('/:author_id', (req, res) => {
  let author_id = req.params.author_id;

  db.query('SELECT * FROM authors WHERE author_id = ?', author_id, (err, results, fields) => {
    if (!err) {
      console.log('[+] Obtaining author...');
      res.json(results);
    } else {
      console.error(`[!] Error: ${err}`);
    }
  });
});

// handles record updates for the specified author id
router.put('/:author_id', (req, res) => {
  let author_id = req.params.author_id;

  db.query('UPDATE authors SET ? WHERE author_id = ?', [req.body, author_id], (err, results) => {
    if (!err) {
      console.log('[+] Author updated successfully!');
      res.json({ message: 'Book updated successfully!' });
    } else {
      console.error(`[!] Error: ${err}`);
    }
  });
});

// handles deletion of a book record
router.delete('/:author_id', (req, res) => {
  let author_id = req.params.author_id;

  db.query('DELETE FROM authors WHERE author_id = ?', author_id, (err, results) => {
    if (!err) {
      console.warn('[-] Author has been deleted!');
      res.json( { message: 'Author has been deleted!' });
    } else {
      console.error(`[!] Error: ${err}`);
    }
  });
});

// module exports for the router
module.exports = router;