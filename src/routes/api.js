'use strict';

// import dependences
const express = require('express');
const router = express.Router();

// some db testing
router.get('/', (req, res) => {
  res.json({ message: 'API is available... Go to /api/test for connectivity tests...' });
});

// just in case i need to test availablility for the api
router.get('/test', (req, res) => {
  res.json({ api: true });
});

// export the module
module.exports = router;
