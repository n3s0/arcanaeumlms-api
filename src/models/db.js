'use strict';

// import dependencies
const config = require('../config');
const mysql = require('mysql');

// create the connection for the database
let db = mysql.createConnection({
  host: config.db.host,
  user: config.db.user,  
  password: config.db.password,
  database: config.db.name
});

// connect to the database and provide error if thrown
db.connect((err) => {
  if (!err) {
    console.log(`[+] Database has connected successfully! Connected to ${config.db.name} on ${config.db.host}:3306 as ${config.db.user}...`);
  } else {
    console.error('[!] Error: Unable to connect to the Mysql database...');
    console.error('[!] Providing Error:');
    console.error(`[!] ${err}`);
  }
});

// export the db module
module.exports = db;
