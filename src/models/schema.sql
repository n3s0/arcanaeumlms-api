/*
  Arcanaeum LMS Database Schema
  Author: Timothy (n3s0)
  This is the database schema for Arcanaeum LMS.

  Schema was created to work with MySQL and MariaDB. Please do not use with other databases.

  If you would like to run this file as a script without all of the other work. Please uncomment the code that is commented out.

  If you have already completed the code commented out below, I would recommend populating the database using the following command.

  npm run db_populate
*/

/*
CREATE DATABASE IF NOT EXISTS lmsdb DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_bin;
CREATE USER IF NOT EXISTS 'lmsdb-admin'@'localhost' IDENTIFIED BY 'S3curity#';
GRANT ALL PRIVILEGES ON lmsdb.* TO 'lmsdb-admin'@'localhost';
*/

CREATE TABLE IF NOT EXISTS books (
  book_id INT NOT NULL AUTO_INCREMENT,
  book_title VARCHAR(100) NOT NULL,
  book_authors VARCHAR(100),
  book_summary VARCHAR(255),
  book_publisher VARCHAR(100),
  book_publish_date VARCHAR(10),
  book_genre VARCHAR(50),
  book_ISBN VARCHAR(20),
  book_read TINYINT(1),
  book_available TINYINT(1),
  PRIMARY KEY (book_id)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS authors (
  author_id INT NOT NULL AUTO_INCREMENT,
  author_name VARCHAR(100) NOT NULL,
  PRIMARY KEY (author_id)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS publishers (
  publisher_id INT NOT NULL AUTO_INCREMENT,
  publisher_name VARCHAR(100) NOT NULL,
  PRIMARY KEY (publisher_id)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS genres (
  genre_id INT NOT NULL AUTO_INCREMENT,
  genre_name VARCHAR(100),
  PRIMARY KEY (genre_id)
) ENGINE=InnoDB;

/*
  Below is commented out because these tables aren't being used just yet.

  They will be used when they have been implemented into the application.

  If you would like to start developing this, you are free to uncomment and play with it.
*/
/*
CREATE TABLE IF NOT EXISTS users (
  user_id INT NOT NULL AUTO_INCREMENT,
  user_name VARCHAR(100) NOT NULL,
  user_first_name VARCHAR(100),
  user_last_name VARCHAR(100),
  user_email VARCHAR(100),
  PRIMARY KEY (user_id)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS groups (
  group_id INT NOT NULL AUTO_INCREMENT,
  group_name VARCHAR(30),
  PRIMARY KEY (group_id)
) ENGINE=InnoDB;
*/
