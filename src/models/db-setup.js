'use strict';

// dependencies
const config = require('../models/config');
const dbsetup = require('commander');
const db = require('./db');

dbsetup
	.version('1.0.0', '-v, --version')
	.usage('[OPTIONS]')
	.command('i, init <db-method>', 'Intialize your database from database file')
	.action(function () {
		
	});

dbsetup.parse(process.argv);