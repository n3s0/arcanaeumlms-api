'use strict';

// dependencies
const express = require('express');
const bodyParser = require('body-parser');
const config = require('./src/config');
const cors = require('cors');
const api = express();

// logs the API requests
let logger = function (req, res, next) {
  console.log(`[+] Request: ${req.protocol} ${req.method} ${req.hostname} ${req.originalUrl}`);
  next();
}

// use the logger
api.use(logger);

// bodyParser() config
api.use(bodyParser.urlencoded({ extended: true }));
api.use(bodyParser.json());
api.use(cors());

// require route files
const api_r = require('./src/routes/api');
const books_r = require('./src/routes/books');
const authors_r = require('./src/routes/authors');
const publishers_r = require('./src/routes/publishers');
const genres_r = require('./src/routes/genres');

// API requests will be prefixed with /api
api.use('/api', api_r);
api.use('/api/books', books_r);
api.use('/api/authors', authors_r);
api.use('/api/publishers', publishers_r);
api.use('/api/genres', genres_r);

// api listener -- edit the port in api/config.js if change is needed
api.listen(config.server.port, () => {
  console.log(`[+] API server is running on http://localhost:${config.server.port}/api/...`)
});
