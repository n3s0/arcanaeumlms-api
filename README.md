# Arcanaeum LMS API

## Written by Timothy (n3s0)

### Introduction

This is the API for Arcanaeum LMS. It is used to handle the REST operation for the application.

#### Development

Since the project isn't production worthy just yet. At this time there is only development documentation.

##### Requirements

This section describes the requirments for the development environment.

- Node.js v12.4.0
- Npm v6.9.0
- MySQL
- API dependencies can be found in the ```./package.json``` file.

##### Clone The Repository

You can clone the repository by issuing the following command.

```shell
git clone https://gitlab.com/n3s0/arcanaeumlms-api.git
```

##### Install Dependencies & Start Server

Then, when you want to run the development environment you can issue the following npm commands.

```shell
cd arcanaeumlms-api/
npm install
npm run startdev
```
